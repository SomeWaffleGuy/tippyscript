# TippyScript
Messy Fedora setup scripts, massively WIP

Should (mostly) work and provide a basic useable system

### Instructions for Fedora Workstation

```
wget https://gitlab.com/SomeWaffleGuy/tippyscript/-/raw/master/workstation.sh
chmod +x workstation.sh
./workstation.sh
```

### Instructions for Fedora Silverblue

```
wget https://gitlab.com/SomeWaffleGuy/tippyscript/-/raw/master/silverblue-1.sh
chmod +x silverblue-1.sh
./silverblue-1.sh
FOLLOW ALL INSTRUCTIONS, THE SYSTEM WILL NEED REBOOTS
```

# Configuration Tips
### NOTE FOR NVIDIA USERS

Under some unknown circumstances, the NVIDIA driver fails to build properly when first installed. If this happens, don't panic, just refer to:

https://rpmfusion.org/Howto/NVIDIA

And uninstall/reinstall the driver to fix it

### NOTE ON FLATHUB
Although the script enables full Flathub instead of the 'Fedora Selection' it does not turn the repository on by default. There is likely a way to do this but it's not worth it with the proposal for full Flathub by default in Fedora 37.
Simply enable 'Fedora Flathub Selection' repo in GNOME Software to access the full Flathub repo  (yes the naming is annoying).

### SecureBoot with akmods

As of Fedora 36, it is possible to use SecureBoot properly with akmods (e.g. the NVIDIA driver), please see the following RPM Fusion page on how to set it up if necessary:

https://rpmfusion.org/Howto/Secure%20Boot

### Celluloid

Celluloid does NOT default to hardware decoding. In order to activate it for Intel/AMD, add the following to 'Extra MPV options'

```
--hwdec=vaapi
```

Or for NVIDIA, add;

```
--hwdec=nvdec
```
If this doesn't work, instead use;

```
--hwdec=vdpau
```

Any standard MPV options can be added this way, or with a config file.

### Firefox

Firefox supports hardware acceleration on all major GPUs (thanks to nvidia-vaapi-driver);

https://wiki.archlinux.org/index.php/Firefox#Hardware_video_acceleration

For more information.

### Extensions

By default, this script installs several useful extensions and disables the 'Background logo' extension. Any and all extensions can be enabled/disabled via the installed Extensions application. Many extensions are available in the Fedora repositories, with many more at;

https://extensions.gnome.org/

### High Refresh Rate with NVIDIA (More than 60 Hz)

In mixed refresh rate setups, add;
```
export __GL_SYNC_DISPLAY_DEVICE=DFP-0
```

Where DFP-0 is the output of your higher refresh rate monitor (DP-4 or HDMI-2 for example).

GSYNC, while supported, can only be used on one monitor at a time on X.org. Wayland has working multimonitor VRR, but GSYNC support is not yet implimented. For more information see;

https://wiki.archlinux.org/index.php/Variable_refresh_rate

https://wiki.archlinux.org/title/NVIDIA

### Flatpak Applications

When installing Flatpak applications, often times there will be a 'Fedora' and a 'Flathub' version. The 'Flathub' versions are more often up-to-date and, as such, I would recommend prefering them.

### Themes

Theming is a waste of time and Adwaita/Adwaita-Dark look fine.

But if you insist, Fedora does have various themes in its repos, and many more out there through build systems.

Be sure to install the Flatpak version of whatever theme you apply for a consistent look.

### KDE Script

This script is provided "as is" to an even greater extent than the rest. I don't use KDE, but it should work fine.

# Additional Scripts
If you use Debian, check out;

https://gitlab.com/SomeWaffleGuy/debscript
