#/bin/bash
echo "$(tput setaf 2)$(tput bold)This is part 3 of the Silverblue setup scrip. DO NOT continue unless you have run silverblue-2.sh$(tput sgr 0)"
echo -n "$(tput setaf 2)$(tput bold)Continue?
(y/N)
$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
    echo "$(tput setaf 2)$(tput bold)Installing typical applications...$(tput sgr 0)"
    sudo rpm-ostree install gnome-tweaks gnome-shell-extension-appindicator libappindicator-gtk3 ffmpeg-libs libva-utils ffmpegthumbnailer neofetch mesa-demos smbios-utils lm_sensors util-linux-user
    echo "$(tput setaf 2)$(tput bold)The following applications are recomended to install with rpm-ostree over Flatpak at  this time;$(tput sgr 0)"
    echo -n "$(tput setaf 2)$(tput bold)Install system Steam? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo rpm-ostree install steam protontricks winetricks
  fi
echo -n "$(tput setaf 2)$(tput bold)Install system Nextcloud Client? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo rpm-ostree install nextcloud-client nextcloud-client-nautilus
  fi
 echo -n "$(tput setaf 2)$(tput bold)Install OpenRGB? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo rpm-ostree install openrgb
  fi 
  echo -n "$(tput setaf 2)$(tput bold)Install zsh (you'll need to manually switch the shell after a reboot)? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
      sudo rpm-ostree install zsh zsh-autosuggestions zsh-syntax-highlighting
    echo -n "$(tput setaf 2)$(tput bold)Use the grml config? 
(y/N)$(tput sgr 0) "
  read answer
    if echo "$answer" | grep -iq "^y" ;then
      wget -O .zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
      sudo wget -O /root/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
    fi
  fi
echo -n "$(tput setaf 2)$(tput bold)Select your primary web browser
1: Firefox
2: Chrome
3: Chromium
4: Other (Manual Setup)
$(tput sgr 0)"
  read answer
  if echo "$answer" | grep -iq "^1" ;then
    sudo rpm-ostree install mozilla-openh264
  elif echo "$answer" | grep -iq "^2" ;then
    sudo rpm-ostree install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
  elif echo "$answer" | grep -iq "^3" ;then
    sudo rpm-ostree install chromium-freeworld
  fi
  echo "$(tput setaf 2)$(tput bold)Making GNOME more useable and enabling Flathub...$(tput sgr 0)"
    gnome-extensions disable background-logo@fedorahosted.org
   gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
    gsettings set org.gnome.desktop.interface clock-show-date 'true'
    gsettings set org.gnome.desktop.interface clock-show-weekday 'true'
    gsettings set org.gnome.desktop.interface enable-hot-corners 'false'
    gsettings set org.gnome.desktop.interface show-battery-percentage 'true'
    gsettings set org.gnome.nautilus.preferences show-create-link 'true'
    gsettings set org.gnome.nautilus.preferences thumbnail-limit '4096'
    gsettings set org.gnome.nautilus.icon-view default-zoom-level 'standard'
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  echo -n "$(tput setaf 2)$(tput bold)Use dark theme? 
(y/N)$(tput sgr 0) "
    read answer
    if echo "$answer" | grep -iq "^y" ;then
      gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"
     # This throws an error on older Fedora versions but doesn't cause issues
      gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
    fi
   echo -n "$(tput setaf 2)$(tput bold)Use 12 hour time? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    gsettings set org.gnome.desktop.interface clock-format '12h'
  fi
  echo -n "$(tput setaf 2)$(tput bold)Enable Tap-to-Click for Touchpads? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click 'true'
  fi
  echo -n "$(tput setaf 2)$(tput bold)Install latest adb/fastboot? 
(y/N)$(tput sgr 0) "
read answer
  if echo "$answer" | grep -iq "^y" ;then
	  git clone https://github.com/M0Rf30/android-udev-rules.git
	  sudo cp ./android-udev-rules/51-android.rules /etc/udev/rules.d/51-android.rules
	  sudo chmod a+r /etc/udev/rules.d/51-android.rules
	  sudo groupadd adbusers
	  sudo usermod -a -G adbusers $(whoami)
	  sudo systemctl restart systemd-udevd.service
	  wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
	  unzip platform-tools-latest-linux.zip
	  sudo cp platform-tools/adb platform-tools/fastboot platform-tools/mke2fs* platform-tools/e2fsdroid /usr/local/bin
	  sudo rm -rf platform-tools platform-tools-latest-linux.zip android-udev-rules
	  wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/adb-updater.sh
	  chmod +x adb-updater.sh
	  #Better method of update?
	echo "$(tput setaf 2)$(tput bold)Use the included adb-updater.sh script to update adb/fastboot as needed. $(tput sgr 0) "
  fi 
fi
echo "$(tput setaf 2)$(tput bold)RESTART REQUIRED TO COMPLETE SETUP
MANUALLY ENABLE INSTALLED EXTENSIONS$(tput sgr 0)"
