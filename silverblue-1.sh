#/bin/bash
#TippyScript Sliverblue: Fedora Silverblue setup script
#CURRENT VERSION: 36
#USE ON NEWER/OLDER VERSIONS AT OWN RISK
echo "$(tput setaf 2)$(tput bold)This script will configure a fresh install of Fedora Silverblue to be what I consider a useable desktop. This includes$(tput sgr 0)$(tput setaf 1)$(tput bold) NON-FREE SOFTWARE AND DRIVERS$(tput sgr 0)$(tput setaf 2)$(tput bold) and suggests software which may be subject to restrictions under local law. DUE TO THE NATURE OF RPM-OSTREE THIS SCRIPT IS IN MULTIPLE PARTS. $(tput sgr 0)"
echo -n "$(tput setaf 2)$(tput bold)Continue?
(y/N)
$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
    wget https://gitlab.com/SomeWaffleGuy/tippyscript/-/raw/master/silverblue-2.sh
    wget https://gitlab.com/SomeWaffleGuy/tippyscript/-/raw/master/silverblue-3.sh
    chmod +x silverblue-2.sh
    chmod +x silverblue-3.sh
    echo "$(tput setaf 2)$(tput bold)Settings up RPM Fusion...$(tput sgr 0)"
    sudo rpm-ostree install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    echo "$(tput setaf 2)$(tput bold)Rebooting, please run silverblue-2.sh after reboot...$(tput sgr 0)"
    sleep 4
    sudo reboot
fi
exit 0
