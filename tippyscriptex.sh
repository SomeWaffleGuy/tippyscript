#/bin/bash
#TippyScript Extra
#Extra sauce for TippyScript
echo -n "$(tput setaf 2)$(tput bold)Install Google Chrome?
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
	sudo dnf -y install google-chrome-stable_current_x86_64.rpm
	sudo rm google-chrome-stable_current_x86_64.rpm
fi
echo -n "$(tput setaf 2)$(tput bold)Install and set adw-gtk3 for consistentcy with libadwaita?
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	sudo dnf -y copr enable nickavem/adw-gtk3
	sudo dnf install -y adw-gtk3
	echo -n "$(tput setaf 2)$(tput bold)1:Dark
2:Light$(tput sgr 0) "
read answer
	if echo "$answer" | grep -iq "^1" ;then
		gsettings set org.gnome.desktop.interface gtk-theme "adw-gtk3-dark"
		gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
	elif echo "$answer" | grep -iq "^2" ;then
		gsettings set org.gnome.desktop.interface gtk-theme "adw-gtk3"
	fi
fi
echo -n "$(tput setaf 2)$(tput bold)Install pipx?
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	sudo dnf install -y python3-pip python3-setuptools python3-libs
	python3 -m pip install --user pipx
	python3 -m pipx ensurepath
fi
echo -n "$(tput setaf 2)$(tput bold)Install latest adb/fastboot? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	sudo dnf -y install git
	git clone https://github.com/M0Rf30/android-udev-rules.git
	sudo cp ./android-udev-rules/51-android.rules /etc/udev/rules.d/51-android.rules
	sudo chmod a+r /etc/udev/rules.d/51-android.rules
	sudo groupadd adbusers
	sudo usermod -a -G adbusers $(whoami)
	sudo systemctl restart systemd-udevd.service
	wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
	unzip platform-tools-latest-linux.zip
	sudo cp platform-tools/adb platform-tools/fastboot platform-tools/mke2fs* platform-tools/e2fsdroid /usr/local/bin
	sudo rm -rf platform-tools platform-tools-latest-linux.zip android-udev-rules
	wget https://gitlab.com/SomeWaffleGuy/debscript/-/raw/master/adb-updater.sh
	chmod +x adb-updater.sh
	#Better method of update?
	echo "$(tput setaf 2)$(tput bold)Use the included adb-updater.sh script to update adb/fastboot as needed. $(tput sgr 0) "
fi
echo -n "$(tput setaf 2)$(tput bold)Install yt-dlp? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	sudo dnf -y install yt-dlp
fi
echo -n "$(tput setaf 2)$(tput bold)Install experimental NVIDIA VA-API driver (read https://github.com/elFarto/nvidia-vaapi-driver for setup)? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	sudo dnf -y copr enable aleasto/nvidia-vaapi-driver
	sudo dnf install nvidia-vaapi-driver
fi
echo -n "$(tput setaf 2)$(tput bold)Install OpenRBG? 
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	wget https://openrgb.org/releases/release_0.7/openrgb_0.7_x86_64_6128731.rpm
	sudo dnf -y install openrgb*.rpm
	rm openrgb*.rpm
	wget https://openrgb.org/releases/release_0.6/60-openrgb.rules
	sudo mv 60-openrgb.rules /etc/udev/rules.d/
	sudo chmod a+r /etc/udev/rules.d/60-openrgb.rules
fi	
echo "$(tput setaf 2)$(tput bold)That's all folks! $(tput sgr 0) "
echo "$(tput setaf 2)$(tput bold)Reboot to be safe. $(tput sgr 0) "
