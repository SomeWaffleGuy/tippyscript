#/bin/bash
echo "$(tput setaf 2)$(tput bold)This is part 2 of the Silverblue setup scrip. DO NOT continue unless you have run silverblue-1.sh$(tput sgr 0)"
echo -n "$(tput setaf 2)$(tput bold)Continue?
(y/N)
$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
    echo -n "$(tput setaf 2)$(tput bold)Select your GPU
1: Intel
2: AMD
3: NVIDIA
4: Other/FOSS Drivers
$(tput sgr 0)"
  read answer
  if echo "$answer" | grep -iq "^1" ;then
    sudo rpm-ostree install intel-media-driver libvdpau-va-gl
  elif echo "$answer" | grep -iq "^2" ;then
    sudo rpm-ostree install xorg-x11-drv-amdgpu libvdpau-va-gl
    echo "$(tput setaf 2)$(tput bold)Additional configuration may be required to use the AMDGPU driver. I do not have a modern AMD GPU for testing.$(tput sgr 0) "
  elif echo "$answer" | grep -iq "^3" ;then
    sudo rpm-ostree install akmod-nvidia xorg-x11-drv-nvidia-cuda xorg-x11-drv-nvidia-cuda-libs vdpauinfo nvidia-vaapi-driver
    sudo wget -O /etc/profile.d/nvidia-vaapi.sh https://gitlab.com/SomeWaffleGuy/tippyscript/-/raw/master/nvidia-vaapi.sh
    sudo rpm-ostree kargs --append=rd.driver.blacklist=nouveau --append=modprobe.blacklist=nouveau --append=nvidia-drm.modeset=1 
  fi
echo "$(tput setaf 2)$(tput bold)Rebooting, please run silverblue-3.sh after reboot...$(tput sgr 0)"
sleep 4
sudo reboot
fi
exit 0
