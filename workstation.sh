#/bin/bash
#TippyScript: Fedora setup script
#CURRENT VERSION: 41
#USE ON NEWER/OLDER VERSIONS AT OWN RISK
echo "$(tput setaf 2)$(tput bold)This script will configure a fresh install of Fedora Workstation to be what I consider a useable desktop. This includes$(tput sgr 0)$(tput setaf 1)$(tput bold) NON-FREE SOFTWARE AND DRIVERS$(tput sgr 0)$(tput setaf 2)$(tput bold) and suggests software which may be subject to restrictions under local law. $(tput sgr 0)"
  echo "$(tput setaf 2)$(tput bold)Uninstalling useless GNOME parts...$(tput sgr 0)"
  sudo dnf -y remove gnome-maps gnome-boxes totem gnome-photos rhythmbox libreoffice*
  echo "$(tput setaf 2)$(tput bold)Checking for system updates...$(tput sgr 0)"
  sudo dnf -y upgrade
  echo "$(tput setaf 2)$(tput bold)Settings up RPM Fusion...$(tput sgr 0)"
  sudo dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  echo "$(tput setaf 2)$(tput bold)Setting up basic multimedia functionality...$(tput sgr 0)"
  sudo dnf -y swap ffmpeg-free ffmpeg --allowerasing
  sudo dnf -y update @multimedia --setopt="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
  echo "$(tput setaf 2)$(tput bold)Installing typical applications...$(tput sgr 0)"
  sudo dnf -y install evolution gnome-tweaks gnome-shell-extension-appindicator libappindicator-gtk3 unrar zip p7zip file-roller curl libva-utils ffmpegthumbnailer fastfetch gstreamer1-vaapi gdouros-symbola-fonts google-noto-emoji-fonts google-noto-emoji-color-fonts google-android-emoji-fonts google-arimo-fonts google-cousine-fonts google-tinos-fonts fira-code-fonts mozilla-fira-sans-fonts mozilla-fira-mono-fonts mesa-demos smbios-utils lm_sensors util-linux-user yt-dlp
  echo -n "$(tput setaf 2)$(tput bold)Install 'tainted' RPM Fusion repositories? 
$(tput sgr 0)$(tput setaf 1)$(tput bold)REPOS CONTAIN SOFTWARE WITH LEGAL RESTRICTIONS $(tput sgr 0)$(tput setaf 2)$(tput bold)
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo dnf -y install rpmfusion-free-release-tainted rpmfusion-nonfree-release-tainted
    echo -n "$(tput setaf 2)$(tput bold)Enable DVD playback? 
$(tput sgr 0)$(tput setaf 1)$(tput bold)LEGAL RESTRICTIONS MAY APPLY (E.g. in the USA)$(tput sgr 0)$(tput setaf 2)$(tput bold) 
(y/N)$(tput sgr 0) "
    read answer
    if echo "$answer" | grep -iq "^y" ;then
      sudo dnf -y install libdvdcss
    fi
    echo -n "$(tput setaf 2)$(tput bold)Install restricted firmware? 
$(tput sgr 0)$(tput setaf 1)$(tput bold)LEGAL RESTRICTIONS MAY APPLY (E.g. in the USA)$(tput sgr 0)$(tput setaf 2)$(tput bold) 
(y/N)$(tput sgr 0) "
    read answer
    if echo "$answer" | grep -iq "^y" ;then
      sudo dnf -y --repo=rpmfusion-nonfree-tainted install "*-firmware"
    fi
  fi
  echo -n "$(tput setaf 2)$(tput bold)Install Gaming Software (Steam, Lutris)? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo dnf -y install steam lutris protontricks
  fi
    echo -n "$(tput setaf 2)$(tput bold)Install Development Tools? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo dnf -y install @development-tools
  fi
  echo -n "$(tput setaf 2)$(tput bold)Install Nextcloud Client? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo dnf -y install nextcloud-client nextcloud-client-nautilus
  fi
  echo -n "$(tput setaf 2)$(tput bold)Install OpenRGB? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    sudo dnf -y install openrgb
  fi
  echo -n "$(tput setaf 2)$(tput bold)Install zsh (does not include zsh-lovers if you want that sort of thing)? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
      sudo dnf -y install zsh zsh-autosuggestions zsh-syntax-highlighting
      chsh -s /bin/zsh
      sudo su -c 'chsh -s /bin/zsh'
    echo -n "$(tput setaf 2)$(tput bold)Use the grml config? 
(y/N)$(tput sgr 0) "
  read answer
    if echo "$answer" | grep -iq "^y" ;then
      wget -O .zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
      sudo wget -O /root/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
    fi
  fi
  echo -n "$(tput setaf 2)$(tput bold)Select your primary web browser
1: Firefox
2: Chrome
3: Chromium
4: Vivaldi
5: Other (Manual Setup)
$(tput sgr 0)"
  read answer
  if echo "$answer" | grep -iq "^1" ;then
    sudo dnf -y install mozilla-openh264
  elif echo "$answer" | grep -iq "^2" ;then
    sudo dnf -y install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
    sudo dnf -y remove firefox
  elif echo "$answer" | grep -iq "^3" ;then
    sudo dnf -y install chromium-freeworld
    sudo dnf -y remove firefox
  elif echo "$answer" | grep -iq "^4" ;then
    sudo dnf config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo
    sudo dnf -y install vivaldi-stable
    sudo dnf -y remove firefox
  fi
  echo -n "$(tput setaf 2)$(tput bold)Select your GPU
1: Intel
2: AMD
3: NVIDIA
4: Other/FOSS Drivers
$(tput sgr 0)"
  read answer
  if echo "$answer" | grep -iq "^1" ;then
    sudo dnf -y install intel-media-driver
  elif echo "$answer" | grep -iq "^2" ;then
    sudo dnf -y swap mesa-va-drivers mesa-va-drivers-freeworld
    sudo dnf -y swap mesa-vdpau-drivers mesa-vdpau-drivers-freeworld
    sudo dnf -y swap mesa-va-drivers.i686 mesa-va-drivers-freeworld.i686
    sudo dnf -y swap mesa-vdpau-drivers.i686 mesa-vdpau-drivers-freeworld.i686
  elif echo "$answer" | grep -iq "^3" ;then
    sudo dnf -y install akmod-nvidia xorg-x11-drv-nvidia-cuda xorg-x11-drv-nvidia-cuda-libs vdpauinfo nvidia-vaapi-driver
    sudo wget -O /etc/profile.d/nvidia-vaapi.sh https://gitlab.com/SomeWaffleGuy/tippyscript/-/raw/master/nvidia-vaapi.sh
  fi
  echo "$(tput setaf 2)$(tput bold)Making GNOME more useable and enabling Flathub...$(tput sgr 0)"
    gnome-extensions disable background-logo@fedorahosted.org
   gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
    gsettings set org.gnome.desktop.interface clock-show-date 'true'
    gsettings set org.gnome.desktop.interface clock-show-weekday 'true'
    gsettings set org.gnome.desktop.interface enable-hot-corners 'false'
    gsettings set org.gnome.desktop.interface show-battery-percentage 'true'
    gsettings set org.gnome.nautilus.preferences show-create-link 'true'
    gsettings set org.gnome.desktop.sound allow-volume-above-100-percent 'true'
    gsettings set org.gnome.nautilus.preferences thumbnail-limit '4096'
    flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
  echo -n "$(tput setaf 2)$(tput bold)Use dark theme? 
(y/N)$(tput sgr 0) "
    read answer
    if echo "$answer" | grep -iq "^y" ;then
      gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
    fi
 echo -n "$(tput setaf 2)$(tput bold)Install and set adw-gtk3 for consistentcy with libadwaita?
(y/N)$(tput sgr 0) "
read answer
if echo "$answer" | grep -iq "^y" ;then
	sudo dnf install -y adw-gtk3-theme
	echo -n "$(tput setaf 2)$(tput bold)1:Dark
2:Light
$(tput sgr 0) "
read answer
	if echo "$answer" | grep -iq "^1" ;then
		gsettings set org.gnome.desktop.interface gtk-theme "adw-gtk3-dark"
		gsettings set org.gnome.desktop.interface color-scheme "prefer-dark"
	elif echo "$answer" | grep -iq "^2" ;then
		gsettings set org.gnome.desktop.interface gtk-theme "adw-gtk3"
	fi
fi
  echo -n "$(tput setaf 2)$(tput bold)Use 12 hour time? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    gsettings set org.gnome.desktop.interface clock-format '12h'
  fi
  echo -n "$(tput setaf 2)$(tput bold)Enable Tap-to-Click for Touchpads? 
(y/N)$(tput sgr 0) "
  read answer
  if echo "$answer" | grep -iq "^y" ;then
    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click 'true'
  fi
echo "$(tput setaf 2)$(tput bold)RESTART REQUIRED TO COMPLETE SETUP
MANUALLY ENABLE INSTALLED EXTENSIONS$(tput sgr 0)"
exit 0
